# -*- encoding: utf-8 -*-
# -*- coding: utf-8 -*-
import arcpy
import os, shutil,json,unicodedata,math,sys,codecs,io#,imgkit
from openpyxl import load_workbook
from arcpy.da import SearchCursor#@UnresolvedImport


'''
Main Class
'''
class FichaPredial:
    FRAME_SLACK_X=2#Holgura en X frames secundarios
    FRAME_SLACK_Y=2#Holgura en Y frames secundarios
    BUILDINGS_FEATURE = "U_CONSTRUCCION"
    PARCEL_FEATURE = "U_TERRENO"
    PARCEL_FEATURE_SIMPLY="U_TERRENO_SIMPLIFICADO"
    PARCEL_LAYER_SIMPLY="terreno_simply.lyr"
    SECTOR_FEATURE = "U_SECTOR"
    ADDRESS_FEATURE = "U_NOMENCLATURA_DOMICILIARIA"
    STREET_NETWORK_FEATURE = "U_NOMENCLATURA_VIAL"
    ADDRESS_LAYER = "address.lyr"
    SECTOR_LAYER = "sector.lyr"
    PARCEL_FEATLAYER = "single_lyrpredio.lyr"
    PARCEL_LOCLAYER = "lyrpredio.lyr"
    STREET_NETWORK_LAYER = "nomenclatura_vial.lyr"
    TEMPLATE_BOUNDARY_DOTS="PUNTOS_LINDERO.lyr"
    TEMP_PARCEL = "temp_parcel"
    TEMP_INTERSECT = "temp_intersect"
    TEMP_BUILDING = "temp_buildings"
    TEMP_STATISTICS = "temp_statistics"
    TMP_POINTS_FEATURE = "points"
    TMP_POINTS_LAYER = "points.lyr"    
    N_PUNTOS_LINDERO_MAX = 18
    WORK_SRID = 3116
    AREAS_BOX_WIDTH=800 #Ancho en pixeles por defecto del cuadro de areas
    AREAS_BOX_FONT_SIZE=30
    def json_load_byteified(self,file_handle):
        return self._byteify(
            json.load(file_handle, object_hook=self._byteify),
            ignore_dicts=True
        )

    def json_loads_byteified(self,json_text):
        return self._byteify(
            json.loads(json_text, object_hook=self._byteify),
            ignore_dicts=True
        )
    
    def _byteify(self,data, ignore_dicts = False):
        # if this is a unicode string, return its string representation
        if isinstance(data, unicode):
            data_clean = unicodedata.normalize('NFKD', data).encode('utf-8','ignore')
            return data_clean
        # if this is a list of values, return list of byteified values
        if isinstance(data, list):
            return [ self._byteify(item, ignore_dicts=True) for item in data ]
        # if this is a dictionary, return dictionary of byteified keys and values
        # but only if we haven't already byteified it
        if isinstance(data, dict) and not ignore_dicts:
            return {
                self._byteify(key, ignore_dicts=True): self._byteify(value, ignore_dicts=True)
                for key, value in data.iteritems()
            }
        # if it's anything else, return it in its original form
        return data
    
    
    '''
    Procesador de ficha predial CAR
    parï¿½metros:
    args:
    1. Configuracion de plantillas
    2. Directorio de Salida, donde se guardaran los pdfs y mxd de salida
    kwargs:
    1. messages: (parï¿½metro nombrado opcional, en caso de que se ejecute desde un tool de arcgis, para que salgan los mensajes en la consola de arcgis).

    '''
    def __init__(self,*args,**kwargs):
        reload(sys)
        sys.setdefaultencoding("ISO-8859-1")#@UndefinedVariable
        self.configs = args[0]
        self.mxdTemplates = self.configs["MXD_TEMPLATES"]
        self.outPutDirectory = self.configs["OUTPUT_DIR"]
        self.workingDirectory= self.configs["WORKING_DIR"]
        self.referenceTemplate = self.configs["REFERENCE_TEMPLATE"]
        self.sde_connection = self.workingDirectory+"\\"+ self.configs["GDB_CONN"]
        if "messages" in kwargs:
            self.messages = kwargs["messages"]
        else :
            self.messages = None
        self.CheckTemplates()
        self.calculateMaxID()

    '''
    Calcula el maximo id para tener la secuencia de lotes
    '''
    def calculateMaxID(self):
        tmp_statistics = self.sde_connection+"\\"+self.TEMP_STATISTICS
        if (arcpy.Exists(tmp_statistics)):
            arcpy.Delete_management(tmp_statistics)
        parcel_lyr = self.sde_connection+"\\"+self.PARCEL_FEATURE
        arcpy.Statistics_analysis(parcel_lyr,tmp_statistics,[["Codigo","MAX"]])
        maxId = 0
        with SearchCursor(tmp_statistics,["MAX_Codigo"]) as cursor:
            for row in cursor:
                maxId = str(int(row[0]))
        if (maxId==0):
            raise Exception("No se puede calcular el max Id interno, verificar el nombre de la tabla de Id Interno en lote ==> Codigo")
        self.maxIDInternall = maxId
        
    '''
    Imprime mensajes en consola
    '''
    def writeMessage(self,msg):
        if self.messages != None:
            self.messages.addMessage(msg)
        else: 
            print (msg)
    
    
 


    '''
    Genera Ficha predial a partir de excel
    '''
    def processAllSetOfFolios(self,xlsx_path):
        folios_to_process=[]
        try:
            workbook = load_workbook(filename=xlsx_path)
            ws = workbook.active
            _min = ws.min_row+1
            _max = ws.max_row
         
            for row in ws.iter_rows(min_row=_min,max_row=_max):
                for cell in row:
                    _val = str(cell.value)
                    if (_val!="" and _val!=None):
                        folios_to_process.append(_val)
        except Exception as e:
            arcpy.AddError("No se pudo procesar excel "+str(e)+e.message)
        for folio in folios_to_process:
            try:
                arcpy.AddMessage("Procesando folio:"+folio)
                info_predial = self.buildInformation(folio)
                arcpy.AddMessage(str(info_predial))
                self.processMxdFile(info_predial)
            except Exception as e:
                arcpy.AddError("No se pudo procesar folio: "+ folio)
                arcpy.AddError(str(e))
        
        
        
        
        
    


 

    '''
    Borra el directorio de salida donde se encuentra el MXD
    '''
    def cleanOutput(self):
        self.writeMessage("Limipiando directorio de salida")
        for filename in os.listdir(self.outPutDirectory):
            file_path = os.path.join(self.outPutDirectory, filename)
            try:
                if os.path.isfile(file_path) or os.path.islink(file_path):
                    os.unlink(file_path)
                elif os.path.isdir(file_path):
                    shutil.rmtree(file_path)
            except Exception as e:
                print('Failed to delete %s. Reason: %s' % (file_path, e))
        self.writeMessage("Directorio de salida")
        
    
    def CheckTemplates(self):
        for template  in self.mxdTemplates:
            mxdTemplateFile = os.path.join(self.workingDirectory,template["MXD_FILE"])
            mxdTemplate =  arcpy.mapping.MapDocument(mxdTemplateFile)
            mapDataFrames = arcpy.mapping.ListDataFrames(mxdTemplate)
            checked = False
            for FRAME in template["FRAMES"]:
                for _mapDataFrame in mapDataFrames:
                        if FRAME["NOMBRE"] == _mapDataFrame.name and FRAME["ID"]=="MAIN":
                            checked = True
                            scale = template["MAX_SCALE"]
                            frame_width = _mapDataFrame.elementWidth/100
                            frame_height = _mapDataFrame.elementHeight/100
                            template["FRAME_WIDTH"] = frame_width 
                            template["FRAME_HEIGHT"] = frame_height 
                            template["MAX_WIDTH"] = scale*frame_width
                            template["MAX_HEIGHT"]= scale*frame_height
            if (checked==False):
                raise Exception("No se pudo checkear el template de origen "+mxdTemplateFile +".Verifique en el fichero de configuracion el nombre del Dataframe Main coincida en el MXD")

    '''
    Determina el Nivel de Zoom y selecciona la plantilla de acuerdo a cual es la primera en la que encaja.
    
    '''
    def selectTemplateByBoundingBox(self,InfoPredial):
        if not "MIN_X" in InfoPredial:
            raise Exception("Hubo un error al calcular el extent del lote especificado, ejecute el tool Add Geometry Attribute Management para ese lote y verifique que la geometria es valida")
        minX = InfoPredial["MIN_X"]
        minY = InfoPredial["MIN_Y"]
        maxX = InfoPredial["MAX_X"]
        maxY = InfoPredial["MAX_Y"]
        deltaX_Parcel = maxX-minX
        deltaY_Parcel = maxY-minY
        curTemplate = None
        continueCheck=True
        for template  in self.mxdTemplates:
            if (curTemplate==None):
                curTemplate = template
            if (continueCheck):
                maxWMap = template["MAX_WIDTH"]
                maxHMap = template["MAX_HEIGHT"]
                if (maxHMap>=deltaY_Parcel and maxWMap>=deltaX_Parcel):
                    curTemplate = template
                    continueCheck = False
        if (continueCheck):
            self.writeMessage("Warning.... Seleccionada plantilla por default")
        return curTemplate
    
    def selectTemplate(self):
        curTemplate = None
        for template  in self.mxdTemplates:
            if (curTemplate==None):
                curTemplate = template
            else:
                break
        return curTemplate
    
    def getNortWestPoint(self,points):
        NWPoint = None
        if (len(points)>0):
            NWPoint = points[0]
        for p in points:
            y2 = NWPoint[1]
            y1 = p[1]
            if (y1>y2):
                NWPoint = p
        for p in points:
            x2 = NWPoint[0]
            y2 = NWPoint[1]
            x1 = p[0]       
            y1 = p[1]
            if (y1==y2 and x1<x2):
                NWPoint = p            
        return NWPoint
    
    def IsClockWise(self,points):
        _isClockWise = False      
        n = len(points)
        _sum = 0;
        for i in range(0,n):
            p1 = points[i]
            p2 = None
            if i==(n-1):            
                p2=points[0]
            else:
                p2=points[i+1]
            x1 = p1[0]
            y1 = p1[1]
            x2 = p2[0]
            y2 = p2[1]
            edge =  (x2-x1)*(y2+y1)
            _sum = _sum+edge            
        if (_sum>=0):
            _isClockWise = True
        return _isClockWise
    
    def SortPoints(self,points):    
        n = len(points)
        NW = self.getNortWestPoint(points)
        isClockWise = self.IsClockWise(points)
        result = [0]*n
        startIndex = None
        for i in range(0,n):
            p = points[i]
            if (p[0]==NW[0] and p[1]==NW[1]):
                startIndex = i
                break
        if isClockWise:
            for cr in range(0,n):
                pos = 0
                if cr < startIndex:
                    pos = n-(startIndex-cr)
                else:
                    pos = cr-startIndex
                result[pos] = points[cr]
        else:
            for cr in range(0,n):
                pos = 0
                if cr <= startIndex:
                    pos = startIndex - cr
                else:
                    pos = n-(cr-startIndex)
                result[pos] = points[cr]
        return result
    
    
    def buildInformation(self,parcel_number):
        feat_parcels = self.sde_connection+"\\"+self.PARCEL_FEATURE
        lyr_parcels = self.workingDirectory+"\\"+self.PARCEL_FEATLAYER
        lyr_complete_parcels = self.workingDirectory+"\\"+self.PARCEL_LOCLAYER
        TMP_POINTS_FEATURE = self.TMP_POINTS_FEATURE+"_"+parcel_number+".shp"
        tmpPoints = self.outPutDirectory+"\\" + TMP_POINTS_FEATURE
        feat_buildings = self.sde_connection+"\\"+self.BUILDINGS_FEATURE
        layer_buildings = self.outPutDirectory+"\\buildings.lyr"
        feat_sector=self.sde_connection+"\\"+self.SECTOR_FEATURE
        layer_sector=self.outPutDirectory+"\\"+self.SECTOR_LAYER
        feat_address=self.sde_connection+"\\"+self.ADDRESS_FEATURE
        layer_address=self.outPutDirectory+"\\"+self.ADDRESS_LAYER
        feat_parcels_simply = self.sde_connection+"\\"+self.PARCEL_FEATURE_SIMPLY
        layer_parcels_simply = self.outPutDirectory+"\\"+self.PARCEL_LAYER_SIMPLY
        feat_street_network = self.sde_connection+"\\"+self.STREET_NETWORK_FEATURE
        layer_street_network = self.outPutDirectory+"\\"+self.STREET_NETWORK_LAYER
        
        if arcpy.Exists(lyr_parcels):
            arcpy.Delete_management(lyr_parcels)
        if arcpy.Exists(tmpPoints):
            arcpy.Delete_management(tmpPoints)
        if arcpy.Exists(lyr_complete_parcels):
            arcpy.Delete_management(lyr_complete_parcels)
        if (arcpy.Exists(layer_sector)):
            arcpy.Delete_management(layer_sector)
        if (arcpy.Exists(layer_address)):
            arcpy.Delete_management(layer_address)
        if (arcpy.Exists(layer_buildings)):
            arcpy.Delete_management(layer_buildings)
        if (arcpy.Exists(feat_parcels_simply)):
            arcpy.Delete_management(layer_parcels_simply)
        if (arcpy.Exists(layer_street_network)):
            arcpy.Delete_management(layer_street_network)
            
        workingSRID = arcpy.SpatialReference(self.WORK_SRID)
        parcel_data={"NUMERO_PREDIAL_VIGENTE":parcel_number,
                     "IDENTIFICADOR_UNICO_PREDIAL":" ",
                     "MATRICULA_INMOBILIARIA":" "}
        parcel_data["DEPARTAMENTO"]=self.configs["DEPARTAMENTO"]
        parcel_data["MUNICIPIO"]=self.configs["MUNICIPIO"]
        parcel_data["ZONA"]=self.configs["ZONA"]
        parcel_data["FECHA_VIGENCIA"]=self.configs["FECHA_VIGENCIA"]
        arcpy.MakeFeatureLayer_management(feat_parcels,lyr_parcels, where_clause = "codigo = '{0}'".format(parcel_number))
        arcpy.MakeFeatureLayer_management(feat_parcels_simply,layer_parcels_simply, where_clause = "codigo = '{0}'".format(parcel_number))
        arcpy.MakeFeatureLayer_management(feat_buildings,layer_buildings, where_clause="Terreno_Codigo='{0}'".format(parcel_number))
        arcpy.MakeFeatureLayer_management(feat_parcels,lyr_complete_parcels)
        arcpy.SelectLayerByLocation_management(lyr_complete_parcels,'BOUNDARY_TOUCHES',lyr_parcels,'', 'NEW_SELECTION')
        arcpy.MakeFeatureLayer_management(feat_sector,layer_sector)
        arcpy.SelectLayerByLocation_management(layer_sector,'INTERSECT',lyr_parcels,'', 'NEW_SELECTION')
        arcpy.MakeFeatureLayer_management(feat_address,layer_address)
        arcpy.SelectLayerByLocation_management(layer_address,'INTERSECT',lyr_parcels,'', 'NEW_SELECTION')
        arcpy.MakeFeatureLayer_management(feat_street_network,layer_street_network)
        sector = " "
        with SearchCursor(layer_sector,["Codigo"]) as cr:
            for r in cr:
                sector = r[0]
        parcel_data["SECTOR"]=sector
        
        
        comuna = " "
        #Pendiente consultar comuna
        parcel_data["COMUNA"]=comuna
        
        neighborhood = " "
        #Pendiente consultar barrio
        parcel_data["BARRIO"]=neighborhood
        
        address = " "
        with SearchCursor(layer_address,["Texto"]) as cr:
            for r in cr:
                address = r[0]
        parcel_data["DIRECCION"]=address
        
        dots = []
        area=0.0
        with SearchCursor(layer_buildings,["SHAPE@"]) as cr:
            for r in cr:
                shape=r[0]
                shape_proj=shape.projectAs(workingSRID)
                area=area+shape_proj.area
        del cr
        constructed_area =  str(round(area, 2))
        parcel_data["AREA_CONSTRUIDA"]=constructed_area
        with SearchCursor(layer_parcels_simply,["CODIGO","SHAPE@","MANZANA_CODIGO","CODIGO_ANTERIOR"]) as crParcel:        
            for r in crParcel:
                shape = r[1]
                parcel_data["MANZANA"]=r[2]
                parcel_data["NUMERO_PREDIAL_ANTERIOR"]=r[3]
                parts = shape.partCount
                shape_proj=shape.projectAs(workingSRID)
                area_formatted = str(round(shape_proj.area,2))
                parcel_data["AREA_DE_TERRENO"]=area_formatted
                for partIndex in range(0,parts):                
                    part = shape.getPart(partIndex)
                    for px in part:
                        X = px.X
                        Y = px.Y
                        dots.append([X,Y])
        
        sortedPoints = self.SortPoints(dots)# sorted(Puntos, key=clockwiseangle_and_distance)
        mainDots = []
        sr =  arcpy.Describe(feat_parcels).spatialReference   
        arcpy.CreateFeatureclass_management(self.outPutDirectory,TMP_POINTS_FEATURE,geometry_type="POINT",spatial_reference= sr)
        arcpy.AddField_management(tmpPoints,"P_LETTER","TEXT",field_length=10)
        arcpy.AddField_management(tmpPoints,"X","DOUBLE")
        arcpy.AddField_management(tmpPoints,"Y","DOUBLE") 
        arcpy.AddField_management(tmpPoints,"DISTANCIA","TEXT",field_length=255)
        arcpy.AddField_management(tmpPoints,"DESDE","TEXT",field_length=255)
        arcpy.AddField_management(tmpPoints,"HASTA","TEXT",field_length=255)
        arcpy.AddField_management(tmpPoints,"COLINDANTE","TEXT",field_length=255)
        i=0
        index_alphabet = 0
        crInsertPoints= arcpy.InsertCursor(tmpPoints)
        a_ascii = 65
        z_ascii = 90    
        alphabet = [0]*((z_ascii-a_ascii)+1)
        for n in range((z_ascii-a_ascii)+1):
            letter = a_ascii+n
            alphabet[n]=chr(letter)
        
        for p in sortedPoints:
            _x0= p[0]
            _y0= p[1]
            point = arcpy.Point(_x0,_y0)
            x1geom = arcpy.PointGeometry(point,sr)
            x1geomPrj = x1geom.projectAs(workingSRID)
            x0 = x1geomPrj.firstPoint.X
            y0 = x1geomPrj.firstPoint.Y
            siguiente_punto = []
            nodo = alphabet[index_alphabet]
            desde = nodo
            hasta = ""
            if i == len(sortedPoints)-1:
                siguiente_punto = sortedPoints[0]
                hasta=alphabet[0]
            else:
                siguiente_punto= sortedPoints[i+1]
                hasta = alphabet[index_alphabet+1]
            _x = siguiente_punto[0]
            _y = siguiente_punto[1]
            pointNext = arcpy.Point(_x,_y)
            x2geomNext = arcpy.PointGeometry(pointNext,sr)
            x2geomPrjNext = x2geomNext.projectAs(workingSRID)
            _a= arcpy.Array([point,pointNext])
            
            polyLine = arcpy.Polyline(_a,sr)
            x = x2geomPrjNext.firstPoint.X
            y = x2geomPrjNext.firstPoint.Y
            
            polyDistProj = polyLine
            
            _length = polyDistProj.length
            
            with SearchCursor(lyr_complete_parcels,["CODIGO","CODIGO_ANTERIOR","SHAPE@"]) as cr:
                for row in cr:
                    colindante = ""
                    compare_code = row[0]
                    code = row[1]
                    geom = row[2]
                    if (compare_code!=parcel_number):
                        #print (code)
                        _intersects = geom.intersect(polyLine,2)
                        _intersects_prj= _intersects.projectAs(workingSRID)
                        if (_intersects_prj.length>0):
                        #if (geom.within(polyLine)):
                            formatted_code = code
                            colindante=formatted_code
                            break
                del cr
            #Check street address if c
            buffer_distance=0.000010
            if (colindante==""):
                arcpy.SelectLayerByAttribute_management(layer_street_network, "CLEAR_SELECTION")
                
                arcpy.SelectLayerByLocation_management(layer_address,'WITHIN_A_DISTANCE',layer_address,buffer_distance, 'NEW_SELECTION')
                with SearchCursor(layer_address,["SHAPE@"]) as cr:
                    for r in cr:
                        _geom = r[0]
                        _geom_buffer  = _geom.buffer(buffer_distance)
                        _crossess_addr = _geom.crosses(polyLine)
                        #_wkt = _geom_buffer.WKT
                        #print(_wkt)
                        if (_crossess_addr):
                            with SearchCursor(layer_street_network,["SHAPE@","TEXTO"]) as crNetwork:
                                for r_st in crNetwork:
                                    _geomStreet = r_st[0]
                                    if (_geom_buffer.crosses(_geomStreet)):
                                        texto = r_st[1]
                                        colindante = texto
            if (colindante==""):
                colindante = 'X'
            distancia = math.sqrt(math.pow(x-x0,2)+math.pow(y-y0,2))
            dist = str(round(distancia, 2))
            if(distancia>0.01):
                mainDots.append({
                    "ID":i,
                    "NODO":nodo,
                    "X":x0,
                    "Y":y0,
                    "DISTANCIA":dist,
                    "DESDE":desde,
                    "HASTA":hasta,
                    "COLINDANTE":colindante                
                    })
                row = crInsertPoints.newRow()
                row.setValue("SHAPE",x1geom)
                row.setValue("P_LETTER",nodo)
                row.setValue("X",x0)
                row.setValue("Y",y0)
                row.setValue("DESDE",desde)
                row.setValue("HASTA",hasta)
                row.setValue("DISTANCIA",dist)
                row.setValue("COLINDANTE",colindante)
                crInsertPoints.insertRow(row)
                index_alphabet=index_alphabet+1
            i=i+1
        del crInsertPoints
        parcel_data["PUNTOS_LINDERO"]=mainDots
        return parcel_data
            
        
        
            

    '''
    Reclibe la informacion predial y realiza el procesamiento de la ficha, genera como resultado un MXD de salida y un 
    pdf 
    '''
    def processMxdFile(self,infoPredial):
        self.writeMessage("Generando Ficha Predial")
        template = self.selectTemplate()
        template_file = os.path.join(self.workingDirectory,template["MXD_FILE"])
        mxdTemplate = arcpy.mapping.MapDocument(template_file)
        layer_template = self.workingDirectory+"\\"+self.TEMPLATE_BOUNDARY_DOTS
        parcel_number=infoPredial["NUMERO_PREDIAL_VIGENTE"]
        TMP_POINTS_FEATURE = self.TMP_POINTS_FEATURE+"_"+parcel_number+".shp"
        TMP_EXCEL_FEATURE = self.TMP_POINTS_FEATURE+"_"+parcel_number+".xls"
        tmpPoints = self.outPutDirectory+"\\" + TMP_POINTS_FEATURE
        tmpExcel = self.outPutDirectory+"\\"+TMP_EXCEL_FEATURE
        lyrPoints = self.outPutDirectory+"\\"+self.TMP_POINTS_LAYER
        if (arcpy.Exists(lyrPoints)):
            arcpy.management.Delete(lyrPoints)
        arcpy.MakeFeatureLayer_management(tmpPoints,lyrPoints)
        arcpy.ApplySymbologyFromLayer_management(lyrPoints,layer_template)
        
        
        #
        
        outPdfFullPath = "{0}\\{1}_{2}.pdf".format(self.outPutDirectory,"PlanoPredial",str(infoPredial["NUMERO_PREDIAL_VIGENTE"]))#Pdf de salida
        outMxdFullPath = "{0}\\{1}_{2}.mxd".format(self.outPutDirectory,"PlanoPredial",str(infoPredial["NUMERO_PREDIAL_VIGENTE"]))#MXD de salida
        #Paso 1 Establecer los valores especificos en los labels del template
        for properties in template["PROPERTY_MAPPING"]:
            elem = properties["MAP_PROPERTY"]
            value_field = properties["DATA_PROPERTY"]
            value = infoPredial[value_field]
            map_elements = arcpy.mapping.ListLayoutElements(mxdTemplate, "TEXT_ELEMENT", elem)
            for mapElement in map_elements:#Establecer la propiedad de texto
                mapElement.text = value
        
        #Paso 2 Procesar puntos Lindero
        npuntos = len(infoPredial["PUNTOS_LINDERO"])
        if (npuntos>self.N_PUNTOS_LINDERO_MAX):
            raise Exception("El numero de puntos lindero es mayor al soportado por la plantilla")
        for i in range(self.N_PUNTOS_LINDERO_MAX):
            nodo = ' '
            x=' '
            y=' '
            desde = ' '
            hasta = ' '
            distancia = ' '
            colindante = ' '
            if i<npuntos:
                punto_lindero = infoPredial["PUNTOS_LINDERO"][i]
                nodo = str(punto_lindero["NODO"])
                x = str(punto_lindero["X"])
                y = str(punto_lindero["Y"])
                desde = str(punto_lindero["DESDE"])
                hasta = str(punto_lindero["HASTA"])
                distancia = str(punto_lindero["DISTANCIA"])
                colindante = str(punto_lindero["COLINDANTE"])
            properties = [
                {"name":"NODO"+str(i),"value":nodo},
                {"name":"X"+str(i),"value":x},
                {"name":"Y"+str(i),"value":y},
                {"name":"DESDE"+str(i),"value":desde},
                {"name":"HASTA"+str(i),"value":hasta},
                {"name":"DISTANCIA"+str(i),"value":distancia},
                {"name":"COLINDANTE"+str(i),"value":colindante}
            ]
            for prop in properties:
                elem = prop["name"]
                value = prop["value"]
                map_elements = arcpy.mapping.ListLayoutElements(mxdTemplate, "TEXT_ELEMENT", elem)
                for mapElement in map_elements:#Establecer la propiedad de texto
                    mapElement.text = value
        #Paso 3. Hacer Zoom adecuado en los diferentes frames:
        mapDataFrames = arcpy.mapping.ListDataFrames(mxdTemplate)
        for FRAME in template["FRAMES"]:
            for _mapDataFrame in mapDataFrames:
                if FRAME["NOMBRE"] == _mapDataFrame.name:
                    ## Capa de Lote
                    layers = arcpy.mapping.ListLayers(mxdTemplate, "", _mapDataFrame)
                    for FRAME_LAYER in FRAME["LAYERS"]:
                        for lyr in layers:
                            if lyr.name == FRAME_LAYER["NOMBRE_CAPA"]:
                                #Establecer Zoom a la capa
                                if FRAME_LAYER["ID"] == "TERRENO":
                                    lyr.definitionQuery = "Codigo = '{0}'".format(infoPredial["NUMERO_PREDIAL_VIGENTE"])
                                    extent = lyr.getSelectedExtent()
                                    
                                    if (FRAME["ID"]=="MAIN"):
                                        dest_Scale =  float(template["MAX_SCALE"])
                                        sizeX = dest_Scale*(_mapDataFrame.elementWidth/100) 
                                        sizeY = dest_Scale*(_mapDataFrame.elementHeight/100)
                                        differenceX = (sizeX-(extent.XMax-extent.XMin))/2
                                        differenceY = (sizeY-(extent.YMax-extent.YMin))/2
                                        xMin = extent.XMin-differenceX
                                        yMin = extent.YMin-differenceY
                                        xMax = extent.XMax+differenceX
                                        yMax = extent.YMax+differenceY
                                        new_Extent = arcpy.Extent(xMin,yMin,xMax,yMax)
                                        _mapDataFrame.extent = new_Extent
                                        addLayer = arcpy.mapping.Layer(lyrPoints)
                                        addLayer.showLabels = True
                                        arcpy.mapping.AddLayer(_mapDataFrame, addLayer, "AUTO_ARRANGE")
                                    #refScale= "1:{0}".format(str(template["MAX_SCALE"]))
                                    #_mapDataFrame.referenceScale = refScale
                                    # _mapDataFrame.scale=int(template["MAX_SCALE"])
                                elif FRAME_LAYER["ID"] == "CONSTRUCCION":
                                    lyr.definitionQuery = "Terreno_Codigo = '{0}'".format(infoPredial["NUMERO_PREDIAL_VIGENTE"])
                                elif FRAME_LAYER["ID"] == "MANZANA":
                                    lyr.definitionQuery = "Codigo = '{0}'".format(infoPredial["MANZANA"])
                                    extent = lyr.getSelectedExtent()
                                    if (FRAME["ID"]=="SECONDARY"):
                                        differenceX=self.FRAME_SLACK_X
                                        differenceY=self.FRAME_SLACK_Y
                                        xMin = extent.XMin-differenceX
                                        yMin = extent.YMin-differenceY
                                        xMax = extent.XMax+differenceX
                                        yMax = extent.YMax+differenceY
                                        new_Extent = arcpy.Extent(xMin,yMin,xMax,yMax)
                                        _mapDataFrame.extent = new_Extent
                            
        #Exportar como pdf y MXD con un template
        arcpy.mapping.ExportToPDF(mxdTemplate,outPdfFullPath)
        mxdTemplate.saveACopy(outMxdFullPath)
        arcpy.TableToExcel_conversion(tmpPoints,tmpExcel)

    '''
    Construye el cuadrito de areas como una imagen y retorna el path de donde se guardÃ³.
    '''
    def buildAreaInfoTable(self,infopredial):
        areaData=infopredial["CUADRO_AREAS"]
        folio = infopredial["LOTE"]
        structureWidth = int(self.AREAS_BOX_WIDTH*0.7)
        areasWidth = int(self.AREAS_BOX_WIDTH*0.3)
        _htmlTemp = u"""<meta charset="UTF-8" />
         <html>
        <head></head>
        <body>
        <table>
        <style>
        table {
          border-collapse: collapse;
        }
        
        table, th, td {
          border: 1px solid black;
          font-size:"""+unicode(self.AREAS_BOX_FONT_SIZE)+u"""px
        }
        </style>
        <table>
        <tr>
            <td colspan="2" style="text-align:center">
                <b>CUADRO DE Ã�REAS</b>
            </td>
        </tr>
        <tr>
            <td style="text-align:center;width:"""+unicode(structureWidth)+u"""px">
                <b>Estructura</b>
            </td>
            <td style="text-align:center;width:"""+unicode(areasWidth)+u"""px">
                <b>Ã�reas  (m<sup>2</sup>)</b>
            </td>
        </tr>
        ${0}
        </table>
        </body>
        </html>
        """
        #_htmlTemplate = codecs.decode(_htmlTemp,"ISO-8859-1")
        _htmlTemplate = _htmlTemp
        _tableContent=""
        for _row in areaData:
            _tableContent=_tableContent+"<tr><td style='text-align:center'>{0}</td><td style='text-align:center'>{1}</td>".format(_row["NOMBRE"],_row["AREA"])
        _html = _htmlTemplate.replace("${0}",_tableContent)
        outfilehtml = "{0}\\cuadro_areas_{1}.html".format(self.outPutDirectory,str(folio))
        outfilepng = "{0}\\cuadro_areas_{1}.png".format(self.outPutDirectory,str(folio))
        with io.open(outfilehtml,"w",encoding="ISO-8859-1") as f:
            f.write(_html)
            f.close()
        options = {
            'format':"png",
            'quiet': '',
            'width':self.AREAS_BOX_WIDTH#'disable-smart-width':True,
        }
        #imgkit.from_file(outfilehtml,outfilepng,options=options)
        return outfilepng
    
    def generateDefaultImg(self,infopredial):
        folio = infopredial["LOTE"]
        _htmlTemplate = u"""<meta charset="UTF-8" />
         <html>
        <head></head>
        <body>
        </body>
        </html>
        """
        #_htmlTemplate = codecs.decode(_htmlTemp,"ISO-8859-1")
        
        outfilehtml = "{0}\\cuadro_areas_{1}.html".format(self.outPutDirectory,str(folio))
        outfilepng = "{0}\\cuadro_areas_{1}.png".format(self.outPutDirectory,str(folio))
        with io.open(outfilehtml,"w",encoding="ISO-8859-1") as f:
            f.write(_htmlTemplate)
            f.close()
        options = {
            'format':"png",
            'quiet': '',
            'width':"1", #'disable-smart-width':True,
            "height":"1"
        }
        #imgkit.from_file(outfilehtml,outfilepng,options=options)
        return outfilepng


    def CalculateScale (self, decScale):
        #x = math.ceil(decScale/factor)
        #newScale = (x)*factor
        if (decScale<=125.0):
            N=25.0
        elif (decScale<=1000.0):
            N=250.0
        else:
            N=1000.0
        _c = float(decScale)
        factor = math.ceil(_c/N)
        newScale = factor*N
        return newScale

    def gridInterval (self,scale):
        cell_length=0.1 #10 cm grid
        return scale*cell_length



'''
ToolBox
'''
class Toolbox(object):
    def __init__(self):
        self.label = "ToolboxExcel"
        self.alias = "ToolboxExcel"

        self.tools = [CreateParcelReportByFolioByRange,CreateParcelReportByFolioId]



'''
Tolbox 1 Crea una sola ficha predial para un predio especifico

'''
class CreateParcelReportByFolioId(object):

    def __init__(self):

        self.label = "Generar Ficha predial por codigo de Lote"
        self.description = "Genera ficha predial para un lote espeficifico"
        self.canRunInBackground = False
        self.Params = {'output_dir':0, "template_config":1,"folio_id":2}
        self.messageController = None



    def getParameterInfo(self):

        theparams = []
        #excelPath
        paramoutputDir = arcpy.Parameter(displayName="Directorio de Salida", name="output_dir", datatype="DEFolder", parameterType="Required", direction="Input")
        theparams.insert(self.Params['output_dir'], paramoutputDir)
        
        paramTemplateConfig = arcpy.Parameter(displayName="Archivo de Configuracion", name="template_config", datatype="DEFile", parameterType="Required", direction="Input")
        paramTemplateConfig.filter.list = ['json']
        theparams.insert(self.Params['template_config'], paramTemplateConfig)
        
        paramFolio= arcpy.Parameter(displayName="Numero de Lote", name="folio_id", datatype="GPString", parameterType="Required", direction="Input")
        theparams.insert(self.Params['folio_id'], paramFolio)
        return theparams

#########
    def isLicensed(self):
        return True

#########
    def updateParameters(self, parameters):
        return

#########
    def updateMessages(self, parameters):
        return

    def execute(self, parameters, messages):
        messages.addMessage("Iniciando Geoproceso")
        outDir = parameters[self.Params['output_dir']].valueAsText
        json = parameters[self.Params["template_config"]].valueAsText
        folio_id = parameters[self.Params["folio_id"]].valueAsText
        _configs = loadJsonFile(json)
        _configs["OUTPUT_DIR"]=outDir
        ficha = FichaPredial(_configs)
        ficha.cleanOutput()
        info_predial = ficha.buildInformation(folio_id)
        messages.addMessage(info_predial)
        ficha.processMxdFile(info_predial)
        return


'''
Tolbox 2 Crea una serie de reportes con un rango de folios

'''
class CreateParcelReportByFolioByRange(object):

    def __init__(self):

        self.label = "Generar Ficha predial por rango de Lotes"
        self.description = "Genera ficha predial para un rango de lotes"
        self.canRunInBackground = False
        self.Params = {'output_dir':0, "template_config":1,"excel":2}
        self.messageController = None



    def getParameterInfo(self):

        theparams = []
        #excelPath
        paramoutputDir = arcpy.Parameter(displayName="Directorio de Salida", name="output_dir", datatype="DEFolder", parameterType="Required", direction="Input")
        theparams.insert(self.Params['output_dir'], paramoutputDir)
        
        paramTemplateConfig = arcpy.Parameter(displayName="Archivo de Configuracion", name="template_config", datatype="DEFile", parameterType="Required", direction="Input")
        paramTemplateConfig.filter.list = ['json']
        theparams.insert(self.Params['template_config'], paramTemplateConfig)
        
        paramExcel = arcpy.Parameter(displayName="Excel", name="excel", datatype="DEFile", parameterType="Required", direction="Input")
        paramExcel.filter.list = ['xlsx']
        theparams.insert(self.Params['excel'], paramExcel)
        return theparams

#########
    def isLicensed(self):
        return True

#########
    def updateParameters(self, parameters):
        return

#########
    def updateMessages(self, parameters):
        return

    def execute(self, parameters, messages):
        messages.addMessage("Iniciando Geoproceso")
        outDir = parameters[self.Params['output_dir']].valueAsText
        json = parameters[self.Params["template_config"]].valueAsText
        _excel = parameters[self.Params["excel"]].valueAsText
        _configs = loadJsonFile(json)
        _configs["OUTPUT_DIR"]=outDir
        ficha = FichaPredial(_configs)
        ficha.cleanOutput()
        ficha.processAllSetOfFolios(_excel)
        return


'''
Encoding and Decoding Helpers
'''
def _decode_list(data):
    rv = []
    for item in data:
        if isinstance(item, unicode):
            item = item.encode('utf-8')
        elif isinstance(item, list):
            item = _decode_list(item)
        elif isinstance(item, dict):
            item = _decode_dict(item)
        rv.append(item)
    return rv

def _decode_dict(data):
    rv = {}
    for key, value in data.iteritems():
        if isinstance(key, unicode):
            key = key.encode('utf-8')
        if isinstance(value, unicode):
            value = value.encode('utf-8')
        elif isinstance(value, list):
            value = _decode_list(value)
        elif isinstance(value, dict):
            value = _decode_dict(value)
        rv[key] = value
    return rv

def loadJsonFile(filename):
        with open (filename,'r') as _file:
            _str = _file.read()
            a = codecs.decode(_str,"ISO-8859-1")
            _unidict = json.loads(a)
        return _decode_dict(_unidict)