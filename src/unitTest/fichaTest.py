# -*- encoding: utf-8 -*-
# -*- coding: utf-8 -*-
import unittest
import os
from fichaPredial import FichaPredial, loadJsonFile


'''
Clase para realizar pruebas unitarias sobre fichaCAR

'''
class FichaCarTesting(unittest.TestCase):
    
    def __init__(self,args):
        unittest.TestCase.__init__(self,args)
        try:
            self.path  = os.path.dirname(os.path.abspath(__file__))
            self.configs = loadJsonFile(os.path.join(self.path,"config.json")) 
            self.dataUnitTest= loadJsonFile(os.path.join(self.path,"dataUnitTest.json"))
            self.miscTest= loadJsonFile(os.path.join(self.path,"miscTest.json"))
        except Exception as e:
            print (e.message)
            raise e.message

    
    def getFicha(self):
      
        ficha = FichaPredial(self.configs)
        return ficha
    
    '''
    Prueba de generacion de MXD y pdf de salida a partir de una estructura de datos identica 
    a la que se debe generar a partir de las consultas sobre los datos.
    Se preuba la plantilla
    '''
    def testProcessMxd(self):
        _assert = False
        try:
            ficha = self.getFicha()
            userData = self.dataUnitTest
            print(userData)
            #ficha.cleanOutput()
            ficha.processMxdFile(userData)
            _assert=True
        except Exception as e:
            print ("Test Process MXD failed --'reason:")
            print(e)
            print (e.message)
        self.assertTrue(_assert)
    
    
    '''
    Prueba la generacion de los puntos y el calculo de los colindantes
    '''
    def testGenerateBoundaryDots(self,):
        _assert = False
        try:
            ficha = self.getFicha()
            userData = self.dataUnitTest
            parcel_number = userData["NUMERO_PREDIAL_VIGENTE"]
            ficha.cleanOutput()
            dots = ficha.buildInformation(parcel_number)
            print(dots)
            _assert=True
        except Exception as e:
            print ("Test Process MXD failed --'reason:")
            print(e)
            print (e.message)
        self.assertTrue(_assert)



    '''
    Prueba de generacion de MXD y pdf de salida a partir de una estructura de datos identica 
    a la que se debe generar a partir de las consultas sobre los datos.
    Se preuba la plantilla
    '''
    def testBuildInfoPredial(self):
        _assert = False
        try:
            ficha = self.getFicha()
            userData = self.dataUnitTest
            testChip = userData["NUMERO_PREDIAL_VIGENTE"]
            ficha.cleanOutput()
            info_predial = ficha.buildInformation(testChip)
            print(info_predial)
            ficha.processMxdFile(info_predial)
            
            _assert=True
        except Exception as e:
            print ("Test Process MXD failed --'reason:")
            print(e)
            print (e.message)
        self.assertTrue(_assert)
        
    
    def testprocessAllFolios(self):
        _assert = False
        try:
            ficha = self.getFicha()
            excel_path = self.miscTest["EXCEL_TEST"]
            ficha.cleanOutput()
            ficha.processAllSetOfFolios(excel_path)
            
            _assert=True
        except Exception as e:
            print ("Test Process XLSX --'reason:")
            print(e)
            print (e.message)
        self.assertTrue(_assert)
